package serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * Classe qui permet l'installation des clients via l'exécution d'un script bash
 */
public class Installation extends Thread
{
	/**
	 * Entier correspondant au dernier octet de l'adresse IP du client à installer.
	 */
	private int i;

	/**
	 * Constructeur par défaut avec le numéro de la machine cliente
	 * @param i Dernier octet de l'IP du client
	 */
	public Installation(int i)
	{
		this.i = i;
	}
	
	/**
	 * Méthode qui permet d'exécuter le code de façon parallèle dans des threads différents
	 */
	public void run()
	{
		try{
	  		String cmd;
			Process p;
			
			cmd = "bash serveur/installation.sh 192.168.0." + i + " client/client.sh";
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();

			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		   	String line = "";
			while ((line = reader.readLine()) != null) {
			   System.out.println(line);
			}
		   
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
